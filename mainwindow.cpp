#include "ui_mainwindow.h"
#include "mainwindow.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPainter>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->boutonGenerer, SIGNAL(clicked()), this, SLOT(creerCodeBarre()));
}
MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * Génère un code-barres à partir du texte spécifié et l'affiche dans le widget "barreGraphique".
 *
 * @note Le texte du code-barres doit être valide et ne peut contenir que des caractères alphanumériques, des espaces
 *       et quelques caractères spéciaux ($ - % . / +).
 */
void MainWindow::creerCodeBarre()
{
    // Récupérer le texte du code-barres à partir de l'éditeur de texte
    QString textEdit = ui->textEdit->toPlainText().trimmed();

    // Vérifier si le code-barres est valide
    if (!estCeQueCodeValide(textEdit)) {
        QMessageBox::warning(this, "Mauvais code Barre", "Le code barre n'est pas valide. Il peut seulement utiliser des caractères alphanumériques, des espaces, et quelques caractères spéciaux ($ - % . / +)");
        return;
    }

    // Ajouter les caractères de début et de fin au texte du code-barres
    QString texteCodeBarre = "*" + textEdit + "*";

    // Calculer la longueur totale du code-barres et la hauteur totale
    int longueurCodeBarre = texteCodeBarre.length();
    int largeurTotal = longueurCodeBarre * 46; // Largeur totale du code à barres
    int hauteurTotal = 80; // Hauteur totale du code à barres

    // Créer une pixmap avec les dimensions spécifiées
    QPixmap pixmapCodeBarre(largeurTotal, hauteurTotal);
    pixmapCodeBarre.fill(Qt::white);

    // Créer un painter associé à la pixmap pour dessiner le code-barres
    QPainter painter(&pixmapCodeBarre);
    painter.setPen(Qt::NoPen);

    int x = 0;

    // Parcourir chaque caractère du code-barres
    for (int i = 0; i < longueurCodeBarre; ++i) {
        QChar currentChar = texteCodeBarre[i];
        int charIndex = CONVERSION.indexOf(currentChar);

        int* codeArray = code39[charIndex];

        // Dessiner la ligne noire au début du caractère
        painter.fillRect(x, 0, longueurCodeBarre, hauteurTotal, Qt::black);
        x += longueurCodeBarre;

        // Dessiner chaque ligne du caractère
        for (int j = 0; j < 9; ++j) {
            if (codeArray[j] == 1) {
                // Dessiner une ligne noire double
                painter.fillRect(x, 0, longueurCodeBarre, hauteurTotal, Qt::black);
            }
            x += longueurCodeBarre;
        }

        // Dessiner la ligne noire à la fin du caractère
        painter.fillRect(x, 0, longueurCodeBarre, hauteurTotal, Qt::black);
        x += longueurCodeBarre;

        // Ajouter une ligne mince blanche entre les caractères
        x += 1;
    }

    // Terminer le dessin sur la pixmap
    painter.end();

    // Afficher la pixmap du code-barres dans le widget "barreGraphique"
    ui->barreGraphique->setPixmap(pixmapCodeBarre);
}


/**
 * Vérifie si le texte spécifié est un code-barres valide.
 *
 * @param texteCodeBarre Le texte à vérifier.
 * @return True si le texte est valide, False sinon.
 *
 * @note Le texte du code-barres est considéré valide s'il ne contient que des caractères alphanumériques, des espaces
 *       et quelques caractères spéciaux ($ - % . / +).
 */
bool MainWindow::estCeQueCodeValide(const QString& texteCodeBarre)
{
    // Liste des caractères valides pour le code-barres
    const QString caracteresValides = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 -$%./+*";

    // Parcourir chaque caractère du texte du code-barres
    for (int i = 0; i < texteCodeBarre.length(); ++i) {
        QChar currentChar = texteCodeBarre[i].toUpper();

        // Vérifier si le caractère courant est valide
        if (!caracteresValides.contains(currentChar))
            return false;
    }

    return true;
}


