# Créateur de code barre

Créateur de code à barre en CODE39 fait en C++

![Capture d'écran PNG sur le créateur de code barre](C:\Users\dbois\OneDrive\Bureau\codeABarre\qt_code_barre\codeBarrePng.png)

## Fonctionnement de l'application

> Entrer le texte à convertir dans la boîte de texte à gauche
>
> Appuyer sur le bouton Generer
>
> Pour effacer, appuyer sur le bouton Effacer
>
> Dans le menu langue il est possible de changer la langue  

###                                                                               Fait par Dale Boisjoli

